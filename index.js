const boards = document.getElementById('boardid')

fetch('https://api.trello.com/1/members/me/boards?key=3c18af412c68f9052ac93198350be38b&token=da3e1d3bfc48dc193d670e817b839cec2f95076bf390ddfa365cb0cd453eca32')
.then(data=>data.json())
.then(data=>allboards(data))


function allboards(boardsinfo){

    for(index=0;index<boardsinfo.length;index++){
        const div1 = document.createElement('div')
        div1.className = 'subdiv1'
        const h3 = document.createElement('h3') 
        div1.style.width='200px'
        div1.style.height='100px'
        div1.style.marginLeft='20px'
        div1.style.marginTop= '30px'
        h3.textContent = boardsinfo[index].name
        h3.style.fontSize='15px'
        h3.style.color = "#fff";
        div1.appendChild(h3)
        if(typeof(boardsinfo[index].prefs.backgroundImage) === "string"){
            imgurl = boardsinfo[index].prefs.backgroundImageScaled[3].url
            div1.style.backgroundImage = `url(${imgurl})`
        }
        else{
            div1.style.backgroundImage = `url('https://venturebeat.com/wp-content/uploads/2021/02/Trello-DashboardView_BlogHero-e1613456680960.png?w=1200&strip=all')`
        }
        
        boards.appendChild(div1)        
    }


    addboard()
    

}








function addboard(){
    
    const btn = document.getElementById('addbtn')
    btn.addEventListener('click',(e)=>{
        e.preventDefault()
        
        const boardname = document.getElementById('boardname')
        console.log(boardname.value)
        
       
        fetch(`https://api.trello.com/1/boards/?name=${boardname.value}&key=3c18af412c68f9052ac93198350be38b&token=da3e1d3bfc48dc193d670e817b839cec2f95076bf390ddfa365cb0cd453eca32`,{
            method:'POST'
        })
        .then(response => {
            console.log(
              `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
          })

          .then(boardData => {
              createTempBoard(boardData)

          })
          .catch(err => console.error(err));
          
    })

}

function createTempBoard(boardData){
    
    const div1 = document.createElement('div')
    const h3 = document.createElement('h3') 
    h3.textContent = boardData.name
    h3.style.fontSize='15px'
    h3.style.color = "#fff";
    div1.appendChild(h3)
    div1.style.width='200px'
    div1.style.height='100px'
    div1.style.marginLeft='20px'
    div1.style.marginTop= '30px'
    div1.style.backgroundColor='blue'
    div1.style.backgroundImage = `url('https://venturebeat.com/wp-content/uploads/2021/02/Trello-DashboardView_BlogHero-e1613456680960.png?w=1200&strip=all')`
    boards.appendChild(div1)        

}

